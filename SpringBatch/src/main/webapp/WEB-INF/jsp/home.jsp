<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
p {text-align: center;}
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}

.left {
  left: 0;
}

.right {
  right: 0;
}

.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}



</style>

<meta charset="ISO-8859-1">
<title>Scheduling</title>
</head>
<body>

 <p style="font-size:50px; color: teal; ">Scheduling Application</p>
 
   <div class="split left">
  
    <div class="centered">

	<h2 style="font-size: 25px; color: navy;">Auto Scheduling</h2>
	<br>
	<a href="autov2"><button type="button" onclick=""
	style="font-size:10pt;color:white;background-color:teal; border:2px solid #336600;padding:3px">Click me</button></a>
	<br>
	</div>
	</div>
	
	
      
    
    <div class="split right">
    <div class="centered">
 
		
	<h2 style="font-size: 25px; color: navy;">Manual Scheduling</h2>
	<br>
	<a href="manualv2"><button type="button" onclick="" style="font-size:10pt;color:white;background-color:teal; border:2px solid #336600;padding:3px">Click
			me</button> </a>

   <br>
   <br>
	<c:if test="${checkManualv2 eq 'manualv2'}">
		<form action="/manualmodev2" method="get">
			<table>
				<c:forEach items="${file}" var="fileName">
					<tr>
						<td><input type="checkbox" name="csvfilev2" value="${fileName}"></td>
						<td>${fileName}</td>
					</tr>
				</c:forEach>
			</table>
			<br>
			<br>

			<b>Select Time: </b> <input type="datetime-local" 	value="yyyy-MM-dd hh:mm:ss" step="1" name="ScheTimev2"
			 min="2020-11-18T00:00"  > 
			<br>
			<br>
			<input	type="submit" value="Submit"  style="font-size:7pt;color:white;background-color:green;border:2px solid #336600;padding:3px">
		</form>
	</c:if>
	
	</div>
	</div>

	
</body>
</html>