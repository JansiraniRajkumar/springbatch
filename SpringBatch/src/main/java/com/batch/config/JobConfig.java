package com.batch.config;

import java.util.HashSet;
import java.util.Set;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import com.batch.processor.EmployeeLineMapper;
import com.batch.writer.DBWriter;

@Configuration
@EnableBatchProcessing
public class JobConfig {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	static Resource[] resources;

	public JobConfig() {
		// TODO Auto-generated constructor stub
	}

	public Resource[] getResources() {

		return resources;
	}

	@SuppressWarnings("static-access")
	public void setResources(Resource[] resources) {
		this.resources = resources;
	}

	@Bean
	@StepScope
	@Qualifier
	public MultiResourceItemReader<String> multiResourceItemReader1() throws Exception {
		MultiResourceItemReader<String> resourceItemReader = new MultiResourceItemReader<String>();
		ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();

		Resource[] resources = {};
		String filePath = "file:";

		resources = patternResolver.getResources(filePath + "C:/Users/jansirani/Documents/CSV1/*.sql");

		Set<Resource> lis = new HashSet<>();
		for (Resource resource : resources) {

			if (resource.getFilename().contains("_")) {
				if (resource.getFilename().contains("_1")) {

					System.out.println("resource names  " + resource.getFilename());

					lis.add(resource);
					System.out.println("added to list");

				}
			} else {

				System.out.println("File not accessed to insert :" + resource.getFilename());

			}

		}

		Resource[] rdf1 = lis.stream().toArray(Resource[]::new);
		System.out.println("length of rdf1 " + rdf1.length);
		resourceItemReader.setResources(rdf1);

		resourceItemReader.setDelegate(reader1());

		return resourceItemReader;

	}

	@Bean
	@StepScope
	@Qualifier
	public MultiResourceItemReader<String> multiResourceItemReader4() throws Exception {

		MultiResourceItemReader<String> resourceItemReader = new MultiResourceItemReader<String>();

		JobConfig spd = new JobConfig();
		resources = spd.getResources();
		for (Resource resource : resources) {
			System.out.println("resource name : " + resource.getFilename());
		}

		resourceItemReader.setResources(resources);
		resourceItemReader.setDelegate(reader1());

		return resourceItemReader;

	}

	

	@Bean
	public FlatFileItemReader<String> reader1() throws Exception {

		FlatFileItemReader<String> reader = new FlatFileItemReader<String>();
		reader.setLineMapper(new EmployeeLineMapper());

		return reader;
	}

	
	

	@Bean
	public DBWriter getWriter() {
		return new DBWriter();
	}

	
	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setMaxPoolSize(10);
		taskExecutor.afterPropertiesSet();
		taskExecutor.getActiveCount();

		return taskExecutor;
	}

	@Bean(name = "autoScheJobv2")
	public Job autoSchJobv2() throws Exception {
		return jobBuilderFactory.get("autoScheJobv2").incrementer(new RunIdIncrementer()).start(step3()).build();
	}

	@Bean(name = "manualScheJobv2")
	public Job job4() throws Exception {
		return jobBuilderFactory.get("manualScheJobv2").start(step4()).build();
	}

	@Bean
	public Step step3() throws Exception {
		return stepBuilderFactory.get("step3").<String, String>chunk(5).reader(multiResourceItemReader1())
				.writer(getWriter()).taskExecutor(taskExecutor()).build();
	}

	@Bean
	public Step step4() throws Exception {
		return stepBuilderFactory.get("step4").<String, String>chunk(5).reader(multiResourceItemReader4())
				.writer(getWriter()).taskExecutor(taskExecutor()).build();
	}

}