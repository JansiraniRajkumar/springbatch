package com.batch.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

import com.batch.model.Employee1;

public class ConsoleItemWriterEmployee1<T> implements ItemWriter<T> {

	@Override
	public void write(List<? extends T> items) throws Exception {
		for (T item : items) {
			Employee1 itemnew = (Employee1) item;
			System.out.println(itemnew.toString());
		}

	}

}